import requests
import json
from flask import Flask
from flask_sqlalchemy import SQLAlchemy


BASE_API = 'https://statsapi.web.nhl.com'


def get_player_stats(playerID, season):
    name = requests.get('{0}/api/v1/people/{1}'.format(BASE_API, playerID))
    response_API = requests.get('{0}/api/v1/people/{1}/stats?stats=statsSingleSeason&season={2}'.format(BASE_API, playerID, season))

    name_data = name.text
    data = response_API.text

    parse_name = json.loads(name_data)
    parse_json = json.loads(data)

    parse_name = parse_name['people']
    for people in parse_name:
        parse_name = people

    parse_json = parse_json['stats']

    for i in parse_json:
        parse_json = i


    for i in parse_json['splits']:
        parse_json = i

    # print(parse_json['stat'])

    print(parse_name['fullName'])
    for i in parse_json['stat']:
        print("{0}: {1}".format(i, parse_json['stat'][i]))


def get_players():
    response_API = requests.get('{0}/api/v1/teams'.format(BASE_API))

    data = response_API.text
    parse_json = json.loads(data)
    teams = parse_json['teams']
    
    teamIds = []
    for team in teams:
        teamIds.append(team['id'])
    
    players = {}
    for teamId in teamIds:
        response_API = requests.get('{0}/api/v1/teams/{1}/roster'.format(BASE_API, teamId))

        data = response_API.text
        parse_json = json.loads(data)
        roster = parse_json['roster']

        count = 0
        for person in roster:
            count += 1
            players[person['person']['id']] = {'fullName':person['person']['fullName'], 'link':person['person']['link']}
        
        for person in players:
            print(person, players[person])


get_player_stats('8480801', '20212022')
# get_players()