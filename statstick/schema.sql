DROP TABLE IF EXISTS user;
DROP TABLE IF EXISTS team;
DROP TABLE IF EXISTS venue;
DROP TABLE IF EXISTS division;
DROP TABLE IF EXISTS conference;
DROP TABLE IF EXISTS franchise;
DROP TABLE IF EXISTS player;
DROP TABLE IF EXISTS profile;
DROP TABLE IF EXISTS user_profile;

CREATE TABLE user (
    id INTEGER PRIMARY KEY AUTOINCREMENT,
    created TIMESTAMP DEFAULT CURRENT_TIMESTAMP,
    updated_at TIMESTAMP DEFAULT CURRENT_TIMESTAMP,
    last_login TIMESTAMP DEFAULT NULL,
    username TEXT UNIQUE NOT NULL,
    email TEXT UNIQUE NOT NULL,
    lower_email TEXT UNIQUE NOT NULL,
    lower_username TEXT UNIQUE NOT NULL,
    password TEXT NOT NULL,
    is_admin BOOLEAN NOT NULL DEFAULT false,
    is_active BOOLEAN NOT NULL DEFAULT true,
    is_banned BOOLEAN NOT NULL DEFAULT false,
    banned_until TIMESTAMP DEFAULT NULL,
    user_profile INTEGER NOT NULL,
    FOREIGN KEY (user_profile) REFERENCES user_profile (id)
);

CREATE TABLE user_profile (
    id INTEGER PRIMARY KEY AUTOINCREMENT,
    username TEXT UNIQUE NOT NULL,
    full_name TEXT NOT NULL,
    first_name TEXT NOT NULL,
    last_name TEXT NOT NULL,
    fav_team INTEGER DEFAULT NULL,
    loc TEXT DEFAULT NULL,
    bio TEXT DEFAULT NULL
);

CREATE TABLE team (
    id INTEGER PRIMARY KEY,
    link TEXT NOT NULL,
    venue INTEGER NOT NULL,
    abbreviation TEXT NOT NULL,
    team_name TEXT NOT NULL,
    location_name TEXT NOT NULL,
    first_year TEXT NOT NULL,
    division INTEGER NOT NULL,
    conference INTEGER NOT NULL,
    franchise INTEGER NOT NULL,
    short_name TEXT NOT NULL,
    FOREIGN KEY (venue) REFERENCES venue (id),
    FOREIGN KEY (division) REFERENCES division (id),
    FOREIGN KEY (conference) REFERENCES conference (id),
    FOREIGN KEY (franchise) REFERENCES franchise (id)
);

CREATE TABLE venue (
    id INTEGER PRIMARY KEY,
    venue_name TEXT NOT NULL,
    link TEXT NOT NULL
);

CREATE TABLE division (
    id INTEGER PRIMARY KEY,
    div_name TEXT NOT NULL,
    short_name TEXT NOT NULL,
    link TEXT NOT NULL,
    abbreviation TEXT NOT NULL,
    conference INTEGER NOT NULL,
    active BOOLEAN NOT NULL,
    FOREIGN KEY (conference) REFERENCES conference (id)
);

CREATE TABLE conference (
    id INTEGER PRIMARY KEY,
    conf_name TEXT NOT NULL,
    link TEXT NOT NULL,
    abbreviation TEXT NOT NULL,
    short_name TEXT NOT NULL,
    active BOOLEAN NOT NULL
);

CREATE TABLE franchise (
    id INTEGER PRIMARY KEY,
    first_year TEXT NOT NULL,
    team_name TEXT NOT NULL,
    location_name TEXT NOT NULL,
    link TEXT NOT NULL
);

CREATE TABLE player (
    id INTEGER PRIMARY KEY,
    full_name TEXT NOT NULL,
    link TEXT NOT NULL,
    first_name TEXT NOT NULL,
    last_name TEXT NOT NULL,
    primary_number INTEGER NOT NULL,
    birth_date TEXT NOT NULL,
    current_age TEXT NOT NULL,
    birth_city TEXT NOT NULL,
    birth_state TEXT NOT NULL,
    birth_country TEXT NOT NULL,
    nationality TEXT NOT NULL,
    height TEXT NOT NULL,
    curr_weight TEXT NOT NULL,
    active BOOLEAN NOT NULL,
    alt_captain BOOLEAN NOT NULL,
    captain BOOLEAN NOT NULL,
    rookie BOOLEAN NOT NULL,
    shoots_catches TEXT NOT NULL,
    roster_status TEXT NOT NULL,
    current_team INTEGER NOT NULL,
    primary_position TEXT NOT NULL,
    FOREIGN KEY (current_team) REFERENCES team (id)
);