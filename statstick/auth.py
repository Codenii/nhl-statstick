from datetime import datetime, timezone
import functools

from flask import (
    Blueprint, flash, g, redirect, render_template, request, session, url_for
)
from werkzeug.security import check_password_hash, generate_password_hash
from statstick.db import get_db

bp = Blueprint('auth', __name__, url_prefix='/auth')


def admin_required(view):
    @functools.wraps(view)
    def wrapped_view(**kwargs):
        if g.user is None:
            error = "Please log in to view this page."
            flash(error)
            return redirect(url_for('auth.login'))
        elif g.user['is_admin'] != True:
            error = "You do not have permissions to view this page."

            flash(error)
            return redirect(url_for('index'))
        
        return view(**kwargs)
    return wrapped_view


def login_required(view):
    @functools.wraps(view)
    def wrapped_view(**kwargs):
        if g.user is None:
            error = "Please log in to view this page."
            flash(error)
            return redirect(url_for('auth.login'))
        
        return view(**kwargs)
    
    return wrapped_view


@bp.before_app_request
def load_logged_in_user():
    user_id = session.get('user_id')

    if user_id is None:
        g.user = None
    else:
        g.user = get_db().execute(
            'SELECT * FROM user WHERE id = ?', (user_id,)
        ).fetchone()
        g.profile = get_db().execute(
            'SELECT * FROM user_profile WHERE id = ?', (g.user['user_profile'],)
        ).fetchone()


@bp.route('/register', methods=('GET', 'POST'))
def register():
    register_active = 'active'
    if request.method == 'POST':
        username = request.form['username']
        email = request.form['email']
        lower_email = email.lower()
        lower_username = username.lower()
        password = request.form['password']
        first_name = request.form['first_name']
        last_name = request.form['last_name']
        fav_team = request.form['fav_team']
        location = request.form['location']
        bio = request.form['bio']
        db = get_db()
        error = None

        if not username:
            error = "Username is required"
        elif not email:
            error = "Email is required"
        elif not password:
            error = "Password is required"
        elif not first_name:
            error = "First name is required"
        elif not last_name:
            error = "Last name is required"
        

        if error is None:
            full_name = "{0} {1}".format(first_name, last_name)
            try:
                db.execute(
                    "INSERT INTO user_profile (username, full_name, first_name, last_name, fav_team, loc, bio) VALUES (?, ?, ?, ?, ?, ?, ?)",
                    (lower_username, full_name, first_name, last_name, fav_team, location, bio),
                )
                db.commit()
            except db.IntegrityError:
                error = "Profile could not be created. Please contact an administrator."


            profile = db.execute(
                "SELECT * FROM user_profile WHERE username = ?", (lower_username,)
            ).fetchone()

            if profile is None:
                error = "Profile was not created. Please contact support."
            else:
                username_check = db.execute(
                    "SELECT * FROM user WHERE lower_username = ?", (lower_username,)
                ).fetchone()
                email_check = db.execute(
                    "SELECT * FROM user WHERE lower_email = ?", (lower_email,)
                ).fetchone()
                
                if username_check is not None:
                    error = f"User {username} is already registered."
                if email_check is not None:
                    error = f"Email {email} is already registered."
            try:
                if error is None:
                    db.execute(
                        "INSERT INTO user (username, email, lower_email, lower_username, password, user_profile) VALUES (?, ?, ?, ?, ?, ?)",
                        (username, email, lower_email, lower_username, generate_password_hash(password), profile['id']),
                    )
                    db.commit()
            except db.IntegrityError as e:
                error = e
            if error is None:
                return redirect(url_for("auth.login"))
            
        flash(error)
    
    return render_template('auth/register.html', register_active=register_active)


@bp.route('/login', methods=('GET', 'POST'))
def login():
    login_active = 'active'
    if request.method == 'POST':
        username = request.form['username']
        lower_username = username.lower()
        password = request.form['password']
        db = get_db()
        error = None
        user = db.execute(
            'SELECT * FROM user WHERE lower_username = ?', (lower_username,)
        ).fetchone()

        if user is None:
            user = db.execute(
                'SELECT * FROM user WHERE lower_email = ?', (lower_username,)
            ).fetchone()
        if user is None:
            error = f"Incorrect Username/Email Address"
        elif not check_password_hash(user['password'], password):
            error = 'Incorrect password'
        
        if error is None:
            session.clear()
            session['user_id'] = user['id']

            db.execute(
                'UPDATE user SET last_login = ? WHERE id = ?',
                (datetime.now(timezone.utc), user['id'],)
            )
            db.commit()
            return redirect(url_for('index'))

        flash(error)
    
    return render_template('auth/login.html', login_active=login_active)


@bp.route('/admin')
@admin_required
def admin():
    admin_active = 'active'
    return render_template('auth/admin.html', admin_active=admin_active)


@bp.route('/user_management')
@admin_required
def user_management():
    um_active = 'active'
    return render_template('auth/user_management.html', um_active=um_active)


@bp.route('/logout')
def logout():
    session.clear()
    return redirect(url_for('index'))
