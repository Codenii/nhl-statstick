import sqlite3

import click
from flask import current_app, g
from flask.cli import with_appcontext
from werkzeug.security import generate_password_hash


def init_app(app):
    app.teardown_appcontext(close_db)
    app.cli.add_command(init_db_command)
    app.cli.add_command(create_superuser_command)


def init_db():
    db = get_db()

    with current_app.open_resource('schema.sql') as f:
        db.executescript(f.read().decode('utf-8'))


@click.command('init-db')
@with_appcontext
def init_db_command():
    '''Clear existing database data, and create new tables.'''
    init_db()
    click.echo('Database Initialized.')

@click.command('create-superuser')
@click.argument('name', nargs=1)
@click.argument('emailadd', nargs=1)
@click.argument('passwd', nargs=1)
@click.argument('fname', nargs=1)
@click.argument('lname', nargs=1)
@with_appcontext
def create_superuser_command(name, emailadd, passwd, fname, lname):
    username = name
    email = emailadd
    password = passwd
    first_name = fname
    last_name = lname
    full_name = "{0} {1}".format(first_name, last_name)
    db = get_db()
    error = None

    try:
        db.execute(
            "INSERT INTO user_profile (username, full_name, first_name, last_name) VALUES (?, ?, ?, ?)",
            (username, full_name, first_name, last_name),
        )
        db.commit()
    except db.IntegrityError:
        error = "Unable to create user profile. Please contact support."

    try:
        profile = get_db().execute(
            "SELECT * FROM user_profile WHERE username = ?", (username,)
        ).fetchone()
        if profile is None:
            error = "Profile was not created. Please contact support."
        else:
            db.execute(
                "INSERT INTO user (username, email, lower_email, lower_username, password, is_admin, user_profile) VALUES (?, ?, ?, ?, ?, true, ?)",
                (username, email, email.lower(), username.lower(), generate_password_hash(password), profile['id']),
            )
            db.commit()
    except db.IntegrityError as e:
        error = e
        user = get_db().execute(
            "SELECT * FROM user WHERE username = ?", (username,)
        )
        if user is None:
            db.execute(
                "DELETE FROM user_profile WHERE username = ?", (username,)
            )
            db.commit()

    if error is not None:
        print(error)
    else:
        click.echo('Superuser Created')


def get_db():
    if 'db' not in g:
        g.db = sqlite3.connect(
            current_app.config['DATABASE'],
            detect_types=sqlite3.PARSE_DECLTYPES
        )
        g.db.row_factory = sqlite3.Row

    return g.db

def close_db(e=None):
    db = g.pop('db', None)

    if db is not None:
        db.close()
