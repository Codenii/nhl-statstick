from flask import (
    Blueprint, flash, g, redirect, render_template, request, url_for
)
from werkzeug.exceptions import abort

from statstick.auth import login_required
from statstick.db import get_db

bp = Blueprint('stats', __name__)


@bp.route('/')
def index():
    db = get_db()
    error = request.args.get('ermsg', None)
    print(error)
    if error is not None:
        flash(error)
    return render_template('stats/index.html')