from datetime import datetime, timezone
import functools

from flask import (
    Blueprint, flash, g, redirect, render_template, request, session, url_for
)
from statstick.db import get_db

from statstick.auth import login_required, admin_required

bp = Blueprint('user', __name__, url_prefix='/user')


@bp.route('/profile/<username>')
def profile_name(username):
    context = {}
    profile_active= 'active'
    db = get_db()

    user = db.execute(
        "SELECT * FROM user WHERE lower_username = ?",
        (username.lower(),)
    ).fetchone()

    if user is None:
        ermsg = "Username does not exist"
        return redirect(url_for('index', ermsg=ermsg))
    else:
        user_profile = db.execute(
            "SELECT * FROM user_profile WHERE id = ?",
            (user['user_profile'],)
        ).fetchone()
    if user_profile is None:
        ermsg = "Username does not exist."
        return redirect(url_for('index', ermsg=ermsg))
    
    else:
        context['id'] = user['id']
        context['username'] = user['username']
        context['email'] = user['email']
        context['full_name'] = user_profile['full_name']
        context['fav_team'] = user_profile['fav_team']
        context['loc'] = user_profile['loc']
        context['bio'] = user_profile['bio']
    return render_template('user/profile.html', profile_active=profile_active, context=context)


@bp.route('/edit_profile', methods=('GET', 'POST'))
@login_required
def edit_profile():
    if request.method == 'POST':
        username = g.user['lower_username']
        email = request.form['email']
        first_name = request.form['first_name']
        last_name = request.form['last_name']
        fav_team = request.form['fav_team']
        location = request.form['location']
        bio = request.form['bio']

        db = get_db()
        error = None

        if not username:
            error = 'No user logged in, please contact support.'
        elif not email:
            email = g.user['email']
        elif not first_name:
            first_name = g.profile['first_name']
        elif not last_name:
            last_name = g.profile['last_name']
        elif not fav_team:
            fav_team = g.profile['fav_team']
        elif not location:
            location = g.profile['fav_team']
        elif not bio:
            bio = g.profile['bio']
        
        
        try:
            db.execute(
                "UPDATE user SET email = ?, lower_email = ? WHERE id = ?",
                (email, email.lower(), g.user['id'])
            )
            db.commit()
        except db.Error as e:
            error = e
        
        if error is None:
            full_name = "{0} {1}".format(first_name, last_name)
            try:
                db.execute(
                    "UPDATE user_profile SET full_name = ?, first_name = ?, last_name = ?, fav_team = ?, loc = ?, bio = ? WHERE id = ?",
                    (full_name, first_name, last_name, fav_team, location, bio, g.user['user_profile'])
                )
                db.commit()
            except db.Error as e:
                error = e
            if error is None:
                return redirect(url_for("user.profile"))
        
        flash(error)

    return render_template('user/edit_profile.html')